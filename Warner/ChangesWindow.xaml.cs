﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExcelDataReader;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace Warner
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class ChangesWindow : Window
    {
        public DataSet ChangesDataSet;
            public DataSet OldDataSet;
             public DataSet NewDataSet;
        public int count = 0;
        public ChangesWindow(DataSet newData,DataSet oldData)
        {
            InitializeComponent();
            OldDataSet = oldData;
            NewDataSet = newData;

            CheckForChanges();
        }
        public void CheckForChanges()
        {
            try
            {
                MChangedDataGrid.Items.Clear();
                
                for (int i = 0; i < OldDataSet.Tables[0].Rows.Count; i++)
                {
                    DataRow oldRow = OldDataSet.Tables[0].Rows[i];
                    DataRow newRow = NewDataSet.Tables[0].Rows[i];
                    if (OldDataSet.Tables[0].Rows.Count == NewDataSet.Tables[0].Rows.Count)
                    {
                        oldRow = OldDataSet.Tables[0].Rows[i];
                        newRow = NewDataSet.Tables[0].Rows[i];
                    }
                    else
                    {
                        if (i == NewDataSet.Tables[0].Rows.Count - 2)
                        {
                            i = OldDataSet.Tables[0].Rows.Count - 1;
                        }
                    }

                    string difCells = "";
                    string difValuesWas = "";
                    string difValuesNow = "";
                    bool isDiff = false;
                    for (int j = 0; j < oldRow.ItemArray.Length; j++)
                    {
                        if (oldRow[j].ToString() != newRow[j].ToString())
                        {
                           // MessageBox.Show(oldRow[j].ToString());
                            difCells += OldDataSet.Tables[0].Columns[j].ColumnName + ";\n";
                            difValuesWas += oldRow[j].ToString() + ";\n";
                            difValuesNow += newRow[j].ToString() + ";\n";
                            isDiff = true;
                        }
                    }
                    if (isDiff)
                    {
                        var row = new ChangesRow(oldRow[0],difCells,difValuesWas,difValuesNow);
                        MChangedDataGrid.Items.Add(row);
                        count++;
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
