﻿using System.Data;

namespace Warner
{
    internal class TableItemRow
    {
        public object Id { set; get; }
        public object Name { set; get; }
        public object Description { set; get; }
        public object Sourse { set; get; }
        public object Object { set; get; }
        public object Confidentiality { set; get; }
        public object Integrity { set; get; }
        public object Availability { set; get; }

        public TableItemRow(DataRow row)
        {
            this.Id = row[0];
            this.Name = row[1];
            this.Description = row[2];
            this.Sourse = row[3];
            this.Object = row[4];

            this.Confidentiality = isNoOrYes(row[5]);

            this.Integrity = isNoOrYes(row[6]);
            this.Availability = isNoOrYes(row[7]);

        }
        private string isNoOrYes(object row)
        {
            return row.ToString() == "0" ?"нет":"да";
            
        }
    }
}