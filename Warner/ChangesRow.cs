﻿namespace Warner
{
    internal class ChangesRow
    {
        public object Id { set; get; }
        public string Changes { set; get; }
        public string Was { set; get; }
        public string Now { set; get; }

        public ChangesRow(object Id, string Changes, string Was, string Now)
        {
            this.Id = Id;
            this.Changes = Changes;
            this.Was = Was;
            this.Now = Now;
        }
    }
}