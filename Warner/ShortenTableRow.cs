﻿namespace Warner
{
    internal class ShortenTableRow
    {
        public object Id { set; get; }
        public object Name { set; get; }
        public ShortenTableRow(object Id, object Name)
        {
            this.Id = Id.ToString();
            this.Name = Name.ToString();
        }
    }
}