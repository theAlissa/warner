﻿using ExcelDataReader;
using Microsoft.Win32;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace Warner
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        DataSet DataBase;

        public bool isSadButtonClicked = false;
        int pageNumber;
        int maxPageNumber=1;
        int rowsOnPage;
        bool UpdatedSuccesfuly = true;
        public MainWindow()
        {
            InitializeComponent();
            rowsOnPage =(int) MDataGrid.Height/21;
            CheckFilesExcistance();
        }
        private void CheckFilesExcistance()
        {
            if (!System.IO.File.Exists("thrlist.xlsx"))
            {
                if (System.Windows.Forms.MessageBox.Show("На вашем компъютере нет необходимого файла.\n Вы согласны на загрузку?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    DownloadFile();
                }
                else { 
                      SadWindow sadWindow= new SadWindow();
                        sadWindow.Show();
                    DownloadFile();
                    System.Windows.MessageBox.Show("А я все равно скачал!");
                    sadWindow.Close();
                }

            }
            else
            {
                DataBase = UpdateData();
                ShowPage(1);
            }
        }

        private void CloseThisStaf(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("Pusssss");
            this.Close();
        }

        private void DownloadFile()
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFile("https://bdu.fstec.ru/files/documents/thrlist.xlsx", "thrlist.xlsx");
                    System.Windows.MessageBox.Show("Файл был скачан");
                }
            }catch(Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString());
            }
        }
        
        public void ShowPage(int number)
        {
            MDataGrid.Items.Clear();
            pageNumber = number;
            DataRow[] rows = DataBase.Tables[0].Select();

            for (int i = rowsOnPage*(number-1); i < rowsOnPage*number; i++)
            {
                if (rows.Length > i)
                {
                    var item = rows[i];
                    var row = new ShortenTableRow(item[0],item[1]);
                    MDataGrid.Items.Add(row);
                }
            }
            MDataGrid.Columns[1].MinWidth = MDataGrid.ActualWidth;
        }
        private DataSet UpdateData()
        {
            DataSet result;
            try
            {
                using (FileStream fs = File.OpenRead("thrlist.xlsx"))
                {
                    using (IExcelDataReader exR = ExcelReaderFactory.CreateReader(fs))
                    {
                        result = exR.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true,
                                ReadHeaderRow = (rowReader) =>
                                {
                                    rowReader.Read();
                                }
                               
                            }
                        }
                        ); ;

                        maxPageNumber = result.Tables[0].Rows.Count / rowsOnPage;
                        if (result.Tables[0].Rows.Count % rowsOnPage != 0)
                        {
                            maxPageNumber++;
                        }
                    }
                }

                return result;
            }

            catch (Exception ex)
            {
                UpdatedSuccesfuly = false;
                System.Windows.MessageBox.Show("Возникла ошибка: "+ex.ToString());
                return null;
            }
        }
        public int CheckChanges(DataSet newData)
        {
            int count = 0;
            ChangesWindow changesWindow = new ChangesWindow(newData,DataBase);
            changesWindow.OldDataSet = DataBase;
            changesWindow.NewDataSet = newData;
            count=changesWindow.count;
            if (count > 0)
            {
                changesWindow.Show();
            }
            System.Windows.MessageBox.Show("Обновление прошло успешно\nОбновлено записей: "+count);
            return count;
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var oldData = DataBase;
            var newData=UpdateData();
            if (UpdatedSuccesfuly)
            {
                CheckChanges(newData);
            }
            DataBase = newData;
            MItemGrid.Items.Clear();
            ShowPage(1);
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = MDataGrid.SelectedItem;
            if (item != null)
            {
                var id = Convert.ToInt32((MDataGrid.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text)-1;
                var row = DataBase.Tables[0].Rows[id];
                MItemGrid.Items.Clear();
                MItemGrid.Items.Add(new TableItemRow(row));
            }
            //MItemGrid.MinRowHeight = MItemGrid.ActualHeight;
        }

        private void Button_Lower_Click(object sender, RoutedEventArgs e)
        {
            if (pageNumber > 1)
            {
                ShowPage(pageNumber - 1);
            }
        }

        private void Button_Higher_Click(object sender, RoutedEventArgs e)
        {

            if (pageNumber != maxPageNumber)
            {
                ShowPage(pageNumber + 1);
            }
            }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            string FileToCopy = "thrlist.xlsx";
            string NewCopy=null;

            if (System.IO.File.Exists(FileToCopy) == true)
            {
                Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
                saveFileDialog.FileName = "file.xlsx";
                
                if (saveFileDialog.ShowDialog() == true)
                {
                    NewCopy = saveFileDialog.FileName;
                    System.Windows.MessageBox.Show(NewCopy);
                }
                try
                {
                    if (NewCopy != null)
                    {
                        if (!System.IO.File.Exists(NewCopy))
                        {

                            System.IO.File.Copy(FileToCopy, NewCopy);
                        }
                        else
                        {
                            System.IO.File.Delete(NewCopy);
                            System.IO.File.Copy(FileToCopy, NewCopy);
                        }
                    }
                }catch (Exception) { }
            }
            else
            {
                System.Windows.MessageBox.Show("NOOOOOOOO THE FILE WAS LOOOOST");
            }
        }
    }


    }
    

